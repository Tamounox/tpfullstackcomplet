import { SpinnerService } from './../services/spinner.service';
import { Component, OnInit } from "@angular/core";
import * as moment from 'moment';
import { ReservationService } from '../reservation.service';

@Component({
  selector: "app-gender",
  templateUrl: "./gender.component.html",
  styleUrls: ["./gender.component.scss"]
})
export class GenderComponent implements OnInit {
  gender = "";
  besoins = [];
  rdv = "";
  hours = "";
  res = {};
  days = '';
  name = '';
  firstname  = '';

  constructor(
    private _reservationService: ReservationService,
    private _spinner: SpinnerService,
  ) {}

  ngOnInit() {}

  getGender(gender) {
    this.gender = gender;
    console.log(this.gender);
  }
  
  getBesoin(besoin) {
    this.besoins.push(besoin);
    console.log(this.besoins);
  }

  getRdv(rdv) {
    this.rdv = rdv;
    console.log(this.rdv);
  }
 
  reservation() {
    this._spinner.presentLoading().then(() => {
      console.log('coucou')

      this._reservationService.reservation({
        gender: this.gender,
        besoin: this.besoins,
        rdv: this.rdv,
        heure: moment(this.hours).format('HH:mm'),
        day: moment(this.days).format('DD/MM'),
        name: this.name,
        firstname: this.firstname
      },() => {
        console.log('dismiss')
        this._spinner.loading.dismiss();
      })
    })

    this.res = {
      gender: this.gender,
      besoin: this.besoins,
      rdv: this.rdv,
      heure: moment(this.hours).format('HH:mm'),
      day: moment(this.days).format('DD/MM'),
      name: this.name,
      firstname: this.firstname
    };

    

  }

}
