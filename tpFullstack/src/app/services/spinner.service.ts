import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  loading: HTMLIonLoadingElement;
  constructor(private loadingController: LoadingController) { }

  async presentLoading(message = 'Chargement...') {
    this.loading = await this.loadingController.create({
      message,
      spinner: 'dots',
      backdropDismiss: false,
      keyboardClose: true,
    });
    await this.loading.present();
  }
}