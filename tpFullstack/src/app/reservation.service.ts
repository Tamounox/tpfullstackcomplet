import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',

  })
};

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  URL = "http://localhost:3001";
  constructor(
    private _http: HttpClient
  ) { }

  reservation(reservation, callback){
    console.log('post')
    console.log(reservation)

    this._http.post(this.URL + '/reservation', reservation, httpOptions).pipe(map(res => res))
    .subscribe(
       response => {
          console.log(response);
          callback();
        },
       error => {console.log(error)
        callback();
     });;
  }

}
