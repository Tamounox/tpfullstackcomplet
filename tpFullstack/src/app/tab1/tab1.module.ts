import { HoursComponent } from './../hours/hours.component';
import { DetailsComponent } from './../details/details.component';
import { HomeComponent } from './../home/home.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { GenderComponent } from '../gender/gender.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: Tab1Page }])
  ],
  declarations: [Tab1Page, HomeComponent, GenderComponent, DetailsComponent, HoursComponent]
})
export class Tab1PageModule {}
